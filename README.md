# SpotifyLogin

# Test automation framework

The framework Launch the spotify Url "https://open.spotify.com/" and automate Login functionality.

-  UC -1 - Test Login form with empty credentials 1.Type any credentials 2.Clear the inputs. 3.Check the error messages: 3.1 Please enter your Spotify username or email address. 3.2 Please enter your password.
- UC-2 - Test Login form with incorrect credentials 1.Type any incorrect credentials and click LOG IN button. 2. Check the error message: 2.1 Incorrect username or password.
- UC -3 - Test Login form with correct credentials 1.Type correct credentials and click LOG IN button. 2.Check that Name is correct.

## Stack

- Java 17.
- JUnit-jupiter.
- Maven.

## Features

- [LoginTest](src/test/java/LoginTest.java) - test itself.
- [Page Objects](src/main/java/com/ta/page) - supplying UI elements locators and page-related methods.
- [DriverManager](src/main/java/com/ta/driver) driver manager has options to choose driver.
- [Model](src/main/java/com/ta/model) data suppliers for the test.
- [resources](src/main/resources) contains test data such.
- [service](src/main/java/com/ta/service) contains extra actions.