package com.ta.model;
public class User {

    private String userLogin;
    private String password;

    public User(String username, String password) {
        this.userLogin = username;
        this.password = password;
    }
    public User() {
        this.userLogin = "";
        this.password = "";
    }
    public String getUsername() {
        return userLogin;
    }

    public String getPassword() {
        return password;
    }
}