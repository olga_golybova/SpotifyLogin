package com.ta.page;

import com.ta.model.User;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage{

    @FindBy(id = "login-username")
    private WebElement inputUserLogin;

    @FindBy(id = "login-password")
    private WebElement inputPassword;

    @FindBy(id = "login-button")
    private WebElement buttonSubmit;
    @FindBy(xpath = "//div[@data-testid='password-error']/span")
    private WebElement passwordError;
    @FindBy(xpath = "//div[@data-testid='username-error']/span")
    private WebElement usernameError;

    @FindBy(xpath = "//div[@data-encore-id='banner']/span")
    private WebElement errorMsgCommon;

    public LoginPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }


    public LoginPage fillUserLogin(User user)
    {
        this.waitVisibilityOfElement(100, inputUserLogin);
        inputUserLogin.sendKeys(user.getUsername());
        inputPassword.sendKeys(user.getPassword());
        return this;
    }
    public MainPage login(User user)
    {
        fillUserLogin(user);
        buttonSubmit.click();
        return new MainPage(driver);
    }

    public LoginPage loginFail(User user)
    {
        fillUserLogin(user);
        buttonSubmit.click();
        return this;
    }

    public String getErrorMessageText() {
        this.waitVisibilityOfElement(100, errorMsgCommon);
        return errorMsgCommon.getText();
    }

    public String getErrorMessageText(String fieldName) {
        WebElement elem;
        if (fieldName.equals("password")){
            elem = passwordError;
        }
        else {
            elem = usernameError;
        }
        this.waitVisibilityOfElement(100, elem);
        return elem.getText();
    }

    public LoginPage cleanField(String fieldName) {
        WebElement elem;
        if (fieldName.equals("password")) {
            elem = inputPassword;
        }
        else {
            elem = inputUserLogin;
        }
        elem.sendKeys(Keys.BACK_SPACE);
        return this;
    }
}