package com.ta.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {
    @FindBy(xpath = "//button[@data-testid='login-button']")
    private WebElement loginButton;

    @FindBy(xpath = "//button[@data-testid='user-widget-link']")
    private WebElement accountBtn;
    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public MainPage openPage(String pageUrl)
    {
        driver.navigate().to(pageUrl);
        return this;
    }

    public LoginPage loginButtonClick() {
        loginButton.click();
        return new LoginPage(driver);
    }

    public String getUserName() {
        this.waitVisibilityOfElement(100, accountBtn);
        return accountBtn.getAttribute("aria-label");
    }
}