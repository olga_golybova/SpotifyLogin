package com.ta.service;

import com.ta.model.User;

public class UserCreator {

    public static User withCredentials(String userLogin, String password){
        return new User(userLogin,
                password);
    }

    public static User withEmptyCredentials(){
        return new User();
    }
}
