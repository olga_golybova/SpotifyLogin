import com.ta.driver.Driver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;

public class CommonConditions {
    protected WebDriver driver;

    @BeforeEach()
    public void setUp()
    {
        driver = Driver.getDriver();
    }

    @AfterEach
    public void stopBrowser()
    {
        Driver.closeDriver();
    }
}
