import com.ta.model.User;
import com.ta.page.MainPage;
import com.ta.service.UserCreator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginTest extends CommonConditions{

    User testUser;
    private final String PAGE_URL = "https://open.spotify.com/";
    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1)
    public void loginWithValidCredentialsTest(String userLogin, String password, String nameExpected) {
        testUser = UserCreator.withCredentials(userLogin, password);

       String userName = new MainPage(driver)
                .openPage(PAGE_URL)
                .loginButtonClick()
                .login(testUser)
                .getUserName();
        assertEquals(nameExpected, userName);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/datainvalid.csv", numLinesToSkip = 1)
    public void loginWithValidInValidCredentialsTest(String userLogin, String password, String errMsgExpected) {
        testUser = UserCreator.withCredentials(userLogin, password);

        String errMsgActual = new MainPage(driver)
                .openPage(PAGE_URL)
                .loginButtonClick()
                .loginFail(testUser)
                .getErrorMessageText();

        assertEquals(errMsgExpected, errMsgActual);
    }

    @ParameterizedTest
    @CsvSource("Incorrect username or password.")
    public void loginWithEmptyCredentialsTest(String errMsgExpected) {
        testUser = UserCreator.withEmptyCredentials();
        String errMsgActual = new MainPage(driver)
                .openPage(PAGE_URL)
                .loginButtonClick()
                .loginFail(testUser)
                .getErrorMessageText();

        assertEquals(errMsgExpected, errMsgActual);
    }

    @ParameterizedTest
    @CsvSource("password,Please enter your password.")
    public void loginWithCleanCredentialFieldTest(String fieldName, String errMsgExpected) {
        testUser = UserCreator.withCredentials("1", "1");

        String errMsgActual = new MainPage(driver)
                .openPage(PAGE_URL)
                .loginButtonClick()
                .fillUserLogin(testUser)
                .cleanField(fieldName)
                .getErrorMessageText(fieldName);

        assertEquals(errMsgExpected, errMsgActual);
    }
}